import os
import re
import shutil
import argparse
import numpy as np
import pandas as pd
import cv2
import nibabel
import albumentations
import torch
import torchvision.transforms as transforms

from models import ENet, UNet, EnsembleModel
from tqdm import tqdm
from os.path import isdir, join


global IMG_SHAPE
global AUGMENTATION
IMG_PATTERN = r'^patient\d+_frame\d+.nii.gz'
MASK_PATTERN = r'^patient\d+_frame\d+_gt.nii.gz'
INFO_PATTERN = 'Info.cfg'
GT_DIR = 'GT'
PRED_DIR = 'Predicted'
SUFFIXES = ['ED', 'ES']

TRANSFORM = transforms.Compose([
    transforms.ToPILImage(),
    transforms.ToTensor(),
    transforms.Normalize(mean=[0.5], std=[0.225])
])


def img2uint8(img):
    img -= img.min()
    img /= img.max()
    img *= 255.
    return img.astype(np.uint8)


def prob2mask(arr, shape):
    pred_mask = np.zeros(shape)
    for cat in range(1, arr.shape[0]):
        mask_cat = arr[cat]
        mask_cat[mask_cat > 0.8] = 1
        mask_cat[mask_cat <= 0.8] = 0
        pred_mask += mask_cat * cat
    assert np.max(pred_mask) <= arr.shape[0] - 1
    return pred_mask


def predict_on_image(img_f, mask_f, model, device):
    img = nibabel.load(img_f)
    if mask_f:
        mask = nibabel.load(mask_f)
        assert img.shape == mask.shape
    else:
        mask = None
    img_arr = img.get_fdata()
    pred_arr = np.zeros_like(img_arr)
    for slice in range(img.shape[2]):
        timg = TRANSFORM(AUGMENTATION(image=img2uint8(img_arr[:, :, slice]))['image'])
        with torch.no_grad():
            p = model(timg.unsqueeze(0).to(device))
            if type(p) is torch.Tensor:
                p = p[0]
            else:
                p = p['out'][0]
        probas = torch.nn.functional.softmax(p, dim=0)
        pred_mask = prob2mask(probas.cpu().numpy(), shape=(probas.shape[1], probas.shape[2]))
        pred_arr[:, :, slice] = albumentations.Resize(img_arr.shape[0], img_arr.shape[1],
                                                      interpolation=cv2.INTER_NEAREST)(image=pred_mask)['image']
    return mask, nibabel.Nifti1Image(pred_arr.astype(np.uint8), affine=img.affine, header=img.header)


def main(acdc_path, out_path, model_path, validation=True):
    device = torch.device('cuda:0') if torch.cuda.is_available() else torch.device('cpu')
    model = torch.load(model_path, map_location=device).eval()
    patients = [d for d in os.listdir(acdc_path) if not d.startswith('.') and isdir(join(acdc_path, d))]
    if validation:
        gt_dir = os.path.join(out_path, GT_DIR)
    pred_dir = os.path.join(out_path, PRED_DIR)
    if validation:
        if not os.path.exists(gt_dir): os.makedirs(gt_dir)
    if not os.path.exists(pred_dir): os.makedirs(pred_dir)
    for patient in tqdm(patients):
        images = []
        masks = []
        for f in sorted(os.listdir(join(acdc_path, patient))):
            if re.findall(IMG_PATTERN, f): images.append(f)
            if validation:
                if re.findall(MASK_PATTERN, f): masks.append(f)
        assert len(images) > 0
        if not validation:
            for i in range(len(images)):
                suffix = SUFFIXES[i]
                _, pred_nifti = predict_on_image(img_f=join(acdc_path, patient, images[i]),
                                                 mask_f=None,
                                                 model=model, device=device)
                pred_nifti.to_filename(join(pred_dir, '%s_%s.nii.gz' % (patient, suffix)))
        else:
            assert len(images) == len(masks)
            for i in range(len(images)):
                suffix = SUFFIXES[i]
                gt_nifti, pred_nifti = predict_on_image(img_f=join(acdc_path, patient, images[i]),
                                                        mask_f=join(acdc_path, patient, masks[i]),
                                                        model=model, device=device)
                gt_nifti.to_filename(join(gt_dir, '%s_%s.nii.gz' % (patient, suffix)))
                pred_nifti.to_filename(join(pred_dir, '%s_%s.nii.gz' % (patient, suffix)))
    return 0


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Script to predict ACDC")
    parser.add_argument("DIR", type=str, help="ACDC dir")
    parser.add_argument("OUTPUT_DIR", type=str, help="Output dir")
    parser.add_argument("PTH_MODEL", type=str, help="PyTorch model")
    parser.add_argument("IMG_SIZE", type=str, help="image size")
    parser.add_argument("MODE", type=str, help="prediction mode")
    args = parser.parse_args()
    assert args.MODE == 'testing' or args.MODE == 'validation'
    validation = True if args.MODE == 'validation' else False

    IMG_SHAPE = (int(args.IMG_SIZE), int(args.IMG_SIZE))
    AUGMENTATION = albumentations.Compose([
        albumentations.CLAHE(p=.7),
        albumentations.Resize(IMG_SHAPE[0], IMG_SHAPE[1]),
    ])

    main(args.DIR, args.OUTPUT_DIR, args.PTH_MODEL, validation=validation)


