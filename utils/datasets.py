import os
import re
import numpy as np
import pandas as pd
import nibabel

import torch
import torch.utils.data
import torchvision.transforms as transforms

from os.path import isdir, join
from typing import Callable, Dict, List, Any


class ACDCDataset(torch.utils.data.Dataset):
    _patient_item_dict: Dict[str, List[Any]]
    CLASSES = {'NOR': 0, 'MINF': 1, 'DCM': 2, 'HCM': 3, 'RV': 4}
    IMG_PATTERN = r'^patient\d+_frame\d+.nii.gz'
    IMG4D_PATTERN = r'^patient\d+_4d.nii.gz'
    MASK_PATTERN = r'^patient\d+_frame\d+_gt.nii.gz'
    INFO_PATTERN = 'Info.cfg'

    def __init__(self, dir_path: str, target_transform: Callable, augmentation, preload=True, load_mask=True):
        super().__init__()
        self.dir_path = dir_path
        self.target_transform = target_transform
        self.augmentation = augmentation
        self._has_masks = load_mask
        self._preload = preload
        self._table = {}
        self._patient_item_dict = {}
        self._patient_cat_dict = {}
        self.patient_split = {}
        self._transform = transforms.Compose([
            transforms.ToPILImage(),
            transforms.ToTensor(),
            transforms.Normalize(mean=[0.5], std=[0.225])
        ])
        assert self.augmentation
        assert self.dir_path
        counter = 0
        patients = [d for d in os.listdir(dir_path) if not d.startswith('.') and isdir(join(dir_path, d))]
        for patient in sorted(patients):
            images = []
            masks = []
            info_file = None
            for f in sorted(os.listdir(join(self.dir_path, patient))):
                if re.findall(self.IMG_PATTERN, f): images.append(f)
                if self._has_masks:
                    if re.findall(self.MASK_PATTERN, f): masks.append(f)
                if re.findall(self.INFO_PATTERN, f): info_file = f
            if self._has_masks:
                assert len(images) == len(masks)
            assert len(images) > 0
            for i in range(len(images)):
                img = nibabel.load(join(self.dir_path, patient, images[i]))
                if self._has_masks:
                    mask = nibabel.load(join(self.dir_path, patient, masks[i]))
                    assert img.shape == mask.shape
                for j in range(img.shape[2]):
                    if self._preload:
                        self._table[counter] = {
                            'img': img.get_fdata()[:, :, j],
                            'mask': mask.get_fdata()[:, :, j] if self._has_masks else None,
                            'meta': pd.read_table(join(self.dir_path, patient, info_file),
                                                  sep=': ', header=None, index_col=0, engine='python').to_dict()[1]
                        }
                    else:
                        self._table[counter] = {
                            'img': join(self.dir_path, patient, images[i]),
                            'mask': join(self.dir_path, patient, masks[i]) if self._has_masks else None,
                            'meta': join(self.dir_path, patient, info_file),
                            'slice': j
                        }
                    if patient in self._patient_item_dict:
                        self._patient_item_dict[patient] += [counter]
                    else:
                        self._patient_item_dict[patient] = [counter]
                    if self._preload:
                        meta_data = self._table[counter]['meta']
                    else:
                        meta_data = pd.read_table(self._table[counter]['meta'], sep=': ', header=None, index_col=0,
                                                  engine='python').to_dict()[1]
                    self._patient_cat_dict[patient] = self.CLASSES[meta_data['Group']] if self._has_masks else 0
                    counter += 1

    def split_train_val(self, train_size=0.3):
        train_indexes = []
        test_indexes = []
        cat_dict = {self.CLASSES[k]: 0 for k in self.CLASSES}
        cat_train_counter = {self.CLASSES[k]: 0 for k in self.CLASSES}
        for patient in sorted(self._patient_cat_dict):
            cat = self._patient_cat_dict[patient]
            cat_dict[cat] += 1
        for patient in sorted(self._patient_cat_dict):
            cat = self._patient_cat_dict[patient]
            if cat_train_counter[cat] < cat_dict[cat] * train_size:
                train_indexes += self._patient_item_dict[patient]
                self.patient_split[patient] = 'train'
                cat_train_counter[cat] += 1
            else:
                test_indexes += self._patient_item_dict[patient]
                self.patient_split[patient] = 'val'
        trainset = torch.utils.data.Subset(self, train_indexes)
        valset = torch.utils.data.Subset(self, test_indexes)
        return trainset, valset

    def __len__(self) -> int:
        return len(self._table)

    def __getitem__(self, item: int) -> (int, np.ndarray, np.ndarray):
        if self._preload:
            meta_data = self._table[item]['meta']
            img = self._table[item]['img']
            img -= img.min()
            img /= img.max()
            img *= 255.
            img = img.astype(np.uint8)
            mask = self._table[item]['mask']
            augmented = self.augmentation(image=img, mask=mask)
        else:
            slice = self._table[item]['slice']
            meta_data = pd.read_table(self._table[item]['meta'], sep=': ', header=None, index_col=0, engine='python').to_dict()[1]
            img = nibabel.load(self._table[item]['img']).get_fdata()[:, :, slice]
            img -= img.min()
            img /= img.max()
            img *= 255.
            img = img.astype(np.uint8)
            mask = nibabel.load(self._table[item]['mask']).get_fdata()[:, :, slice] if self._has_masks else None
            if self._has_masks:
                augmented = self.augmentation(image=img, mask=mask)
            else:
                augmented = self.augmentation(image=img)
        return self.CLASSES[meta_data['Group']] if self._has_masks else 0, \
               self._transform(augmented['image']), \
               torch.as_tensor(augmented['mask'], dtype=torch.int64).unsqueeze(0) if self._has_masks else None


if __name__ == '__main__':

    import pylab as plt
    import albumentations

    augmentation = albumentations.Compose([
        albumentations.CLAHE(p=1.),
        albumentations.GridDistortion(p=1.),
        albumentations.ElasticTransform(p=1.),
        albumentations.OpticalDistortion(p=1.),
    ])

    dataset = ACDCDataset('../data/acdc/training', None, augmentation, preload=False)
    # dataset = ACDCDataset('../data/acdc/testing/testing', None, augmentation, preload=False, load_mask=False)

    c, img, mask = dataset[72]
    plt.imshow(img.numpy()[0])
    plt.imshow(mask.numpy()[0], alpha=0.5)
    plt.show()

    train_dataset, val_dataset = dataset.split_train_val(train_size=0.7)

    for patient in dataset.patient_split:
        print(patient, dataset.patient_split[patient], dataset._patient_cat_dict[patient])
