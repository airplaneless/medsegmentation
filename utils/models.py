import torch
import torch.nn as nn
import torch.nn.functional as F


class EnsembleModel(torch.nn.Module):

    def __init__(self, model_list):
        super().__init__()
        self.models = torch.nn.ModuleList(model_list)

    def forward(self, x):
        o = []
        for m in self.models:
            p = m(x)
            if type(p) is torch.Tensor:
                pass
            else:
                p = p['out']
            o += [p]
        return torch.stack(o).sum(dim=0)


class ENet(nn.Module):
    def __init__(self, n_classes=0):
        super().__init__()

        self.net = nn.ModuleList([
            Downsampler(1, 16),
            Bottleneck(16, 64, 0.01, downsample=True),

            Bottleneck(64, 64, 0.01),
            Bottleneck(64, 64, 0.01),
            Bottleneck(64, 64, 0.01),
            Bottleneck(64, 64, 0.01),

            Bottleneck(64, 128, 0.1, downsample=True),

            Bottleneck(128, 128, 0.1),
            Bottleneck(128, 128, 0.1, dilation=2),
            Bottleneck(128, 128, 0.1, asymmetric_ksize=5),
            Bottleneck(128, 128, 0.1, dilation=4),
            Bottleneck(128, 128, 0.1),
            Bottleneck(128, 128, 0.1, dilation=8),
            Bottleneck(128, 128, 0.1, asymmetric_ksize=5),
            Bottleneck(128, 128, 0.1, dilation=16),

            Bottleneck(128, 128, 0.1),
            Bottleneck(128, 128, 0.1, dilation=2),
            Bottleneck(128, 128, 0.1, asymmetric_ksize=5),
            Bottleneck(128, 128, 0.1, dilation=4),
            Bottleneck(128, 128, 0.1),
            Bottleneck(128, 128, 0.1, dilation=8),
            Bottleneck(128, 128, 0.1, asymmetric_ksize=5),
            Bottleneck(128, 128, 0.1, dilation=16),

            Upsampler(128, 64),

            Bottleneck(64, 64, 0.1),
            Bottleneck(64, 64, 0.1),

            Upsampler(64, 16),

            Bottleneck(16, 16, 0.1),

            nn.ConvTranspose2d(16, n_classes + 1, (2, 2), (2, 2))])

    def forward(self, x):
        max_indices_stack = []

        for module in self.net:
            if isinstance(module, Upsampler):
                x = module(x, max_indices_stack.pop())
            else:
                x = module(x)

            if type(x) is tuple:  # then it was a downsampling bottleneck block
                x, max_indices = x
                max_indices_stack.append(max_indices)

        return x


class Upsampler(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()
        bt_channels = in_channels // 4

        self.main_branch = nn.Sequential(
            nn.Conv2d(in_channels, bt_channels, (1, 1), bias=False),
            nn.BatchNorm2d(bt_channels, 1e-3),
            nn.ReLU(True),

            nn.ConvTranspose2d(bt_channels, bt_channels, (3, 3), 2, 1, 1),
            nn.BatchNorm2d(bt_channels, 1e-3),
            nn.ReLU(True),

            nn.Conv2d(bt_channels, out_channels, (1, 1), bias=False),
            nn.BatchNorm2d(out_channels, 1e-3))

        self.skip_connection = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, (1, 1), bias=False),
            nn.BatchNorm2d(out_channels, 1e-3))

    def forward(self, x, max_indices):
        x_skip_connection = self.skip_connection(x)
        x_skip_connection = F.max_unpool2d(x_skip_connection, max_indices, (2, 2))

        return F.relu(x_skip_connection + self.main_branch(x), inplace=True)


class Downsampler(nn.Module):
    def __init__(self, in_channels, out_channels):
        super().__init__()
        self.conv = nn.Conv2d(in_channels, out_channels - in_channels, (3, 3), 2, 1, bias=False)
        self.bn = nn.BatchNorm2d(out_channels, 1e-3)
        self.prelu = nn.PReLU(out_channels)

    def forward(self, x):
        x = torch.cat((F.max_pool2d(x, (2, 2)), self.conv(x)), 1)
        x = self.bn(x)
        x = self.prelu(x)
        return x


class Bottleneck(nn.Module):
    def __init__(
            self, in_channels, out_channels, dropout_prob=0.0, downsample=False,
            asymmetric_ksize=None, dilation=1, use_prelu=True):

        super().__init__()
        bt_channels = in_channels // 4
        self.downsample = downsample
        self.channels_to_pad = out_channels - in_channels

        input_stride = 2 if downsample else 1

        main_branch = [
            nn.Conv2d(in_channels, bt_channels, input_stride, input_stride, bias=False),
            nn.BatchNorm2d(bt_channels, 1e-3),
            nn.PReLU(bt_channels) if use_prelu else nn.ReLU(True)
        ]

        if asymmetric_ksize is None:
            main_branch += [
                nn.Conv2d(bt_channels, bt_channels, (3, 3), 1, dilation, dilation)
            ]
        else:
            assert type(asymmetric_ksize) is int
            ksize, padding = asymmetric_ksize, (asymmetric_ksize - 1) // 2
            main_branch += [
                nn.Conv2d(bt_channels, bt_channels, (ksize, 1), 1, (padding, 0), bias=False),
                nn.Conv2d(bt_channels, bt_channels, (1, ksize), 1, (0, padding))
            ]

        main_branch += [
            nn.BatchNorm2d(bt_channels, 1e-3),
            nn.PReLU(bt_channels) if use_prelu else nn.ReLU(True),
            nn.Conv2d(bt_channels, out_channels, (1, 1), bias=False),
            nn.BatchNorm2d(out_channels, 1e-3),
            nn.Dropout2d(dropout_prob)
        ]

        self.main_branch = nn.Sequential(*main_branch)
        self.output_activation = nn.PReLU(out_channels) if use_prelu else nn.ReLU(True)

    def forward(self, x):
        if self.downsample:
            x_skip_connection, max_indices = F.max_pool2d(x, (2, 2), return_indices=True)
        else:
            x_skip_connection = x

        if self.channels_to_pad > 0:
            x_skip_connection = F.pad(x_skip_connection, (0, 0, 0, 0, 0, self.channels_to_pad))

        x = self.output_activation(x_skip_connection + self.main_branch(x))

        if self.downsample:
            return x, max_indices
        else:
            return x


class UNet(nn.Module):
    def __init__(self, in_channels=1, n_classes=1, depth=5, wf=5, padding=True,
                 batch_norm=True, up_mode='upconv'):
        """
        Implementation of
        U-Net: Convolutional Networks for Biomedical Image Segmentation
        (Ronneberger et al., 2015)
        https://arxiv.org/abs/1505.04597
        Using the default arguments will yield the exact version used
        in the original paper
        Args:
            in_channels (int): number of input channels
            n_classes (int): number of output channels
            depth (int): depth of the network
            wf (int): number of filters in the first layer is 2**wf
            padding (bool): if True, apply padding such that the input shape
                            is the same as the output.
                            This may introduce artifacts
            batch_norm (bool): Use BatchNorm after layers with an
                               activation function
            up_mode (str): one of 'upconv' or 'upsample'.
                           'upconv' will use transposed convolutions for
                           learned upsampling.
                           'upsample' will use bilinear upsampling.
        """
        super(UNet, self).__init__()
        assert up_mode in ('upconv', 'upsample')
        self.padding = padding
        self.depth = depth
        prev_channels = in_channels
        self.down_path = nn.ModuleList()
        for i in range(depth):
            self.down_path.append(UNetConvBlock(prev_channels, 2**(wf+i),
                                                padding, batch_norm))
            prev_channels = 2**(wf+i)

        self.up_path = nn.ModuleList()
        for i in reversed(range(depth - 1)):
            self.up_path.append(UNetUpBlock(prev_channels, 2**(wf+i), up_mode,
                                            padding, batch_norm))
            prev_channels = 2**(wf+i)

        self.last = nn.Conv2d(prev_channels, n_classes, kernel_size=1)

    def forward(self, x):
        blocks = []
        for i, down in enumerate(self.down_path):
            x = down(x)
            if i != len(self.down_path)-1:
                blocks.append(x)
                x = F.avg_pool2d(x, 2)

        for i, up in enumerate(self.up_path):
            x = up(x, blocks[-i-1])

        return self.last(x)


class UNetConvBlock(nn.Module):
    def __init__(self, in_size, out_size, padding, batch_norm):
        super(UNetConvBlock, self).__init__()
        block = []

        block.append(nn.Conv2d(in_size, out_size, kernel_size=3,
                               padding=int(padding)))
        block.append(nn.ReLU())
        if batch_norm:
            block.append(nn.BatchNorm2d(out_size))

        block.append(nn.Conv2d(out_size, out_size, kernel_size=3,
                               padding=int(padding)))
        block.append(nn.ReLU())
        if batch_norm:
            block.append(nn.BatchNorm2d(out_size))
        block.append(nn.Dropout2d(0.3))
        self.block = nn.Sequential(*block)

    def forward(self, x):
        out = self.block(x)
        return out


class UNetUpBlock(nn.Module):
    def __init__(self, in_size, out_size, up_mode, padding, batch_norm):
        super(UNetUpBlock, self).__init__()
        if up_mode == 'upconv':
            self.up = nn.ConvTranspose2d(in_size, out_size, kernel_size=2,
                                         stride=2)
        elif up_mode == 'upsample':
            self.up = nn.Sequential(nn.Upsample(mode='bilinear', scale_factor=2),
                                    nn.Conv2d(in_size, out_size, kernel_size=1),
                                    nn.Dropout2d(0.3))

        self.conv_block = UNetConvBlock(in_size, out_size, padding, batch_norm)

    def center_crop(self, layer, target_size):
        _, _, layer_height, layer_width = layer.size()
        diff_y = (layer_height - target_size[0]) // 2
        diff_x = (layer_width - target_size[1]) // 2
        return layer[:, :, diff_y:(diff_y + target_size[0]), diff_x:(diff_x + target_size[1])]

    def forward(self, x, bridge):
        up = self.up(x)
        crop1 = self.center_crop(bridge, up.shape[2:])
        out = torch.cat([up, crop1], 1)
        out = self.conv_block(out)

        return out